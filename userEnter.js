function validateForm() {
    var email = document.forms["userForm"]["userName"].value;
    var password = document.forms["userForm"]["userPassword"].value;

    if (email == "" || password == "") {
        console.log("Error in entered data");
        alert("Form should be completed");
        return false;
    } else {
        console.log("Data in form is correctly entered");
        console.log("Email:password -> "+ email + ":" + password);
        alert("Form is correctly completed");
        return true;
    }
}

function createAccount() {
    console.log("User would like to create an account");
    document.location.href='create-account.html';
}